const ParserStream = require('./dist/parser-stream').default

const ps = new ParserStream()

ps.on('data', object => {
  console.log(object.id)
})

ps.on('end', () => {
  console.log('finished')
})

ps.write('{"a": 1, "b": [{"id": 1}, {"id": 2}]}')
ps.end()
