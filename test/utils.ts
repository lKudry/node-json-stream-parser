import Parser from '../src/parser'

export async function testIt (parts: string[], expectObjects: number, buffSize = 50, skip = 0) {
    console.log(`Test [${JSON.stringify(arguments)}] begun.`)
    async function * it () {
        for (let part of parts) {
            yield Buffer.from(part)
        }
    }
    const p = new Parser(it(), buffSize, skip)
    for (let i = 0; i < expectObjects; i++) {
        JSON.parse((await p.nextObject() || "{]").toString())
    }
    if ((await p.nextObject()) !== null) {
        throw Error(`There must be exactly [${expectObjects}] but more found.`)
    }
    console.log(`Test [${JSON.stringify(arguments)}] succeeded.`)
}
