# fast-json-stream-parser
![](https://gitlab.com/svobol13/node-json-stream-parser/badges/master/pipeline.svg)

!! This is early beta, do not use in production !!

JSON parser that works with huge arrays and performs fast. 

As a motivation to build this package was speed of [JSONStream](https://www.npmjs.com/package/JSONStream) package. This package does parsing atleast **twice as fast**.

Current version is so naive that it scans objects on first level. I.e. it finds two objects in this json string `[{},{}]` while it finds single object in this string `{"a":[{}, {}, {}]}`. For more information see tests.
## install
```npm install fast-json-stream-parser```

## example

``` js
const ParserStream = require('fast-json-stream-parser').default

const ps = new ParserStream()

ps.on('data', object => {
  console.log(object.id)
})

ps.on('end', () => {
  console.log('finished')
})

ps.write('{"a": 1, "b": [{"id": 1}, {"id": 2}]}')
ps.end()

```
