import { parentPort } from 'worker_threads'

const td = new TextDecoder()
parentPort.on('message', (buf) => {
  parentPort.postMessage(JSON.parse(td.decode(buf)))
})
