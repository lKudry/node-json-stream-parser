import Parser from './parser-stream'
import * as fs from 'fs'
import { parse } from 'JSONStream'

async function f () {
  let i = 0
  let sec = 0

  function log () {
    console.log(`Processed [${i}] speed [${Math.floor(i / sec++)}] products per second.`)
  }

  const interval = setInterval(log, 1000)

  const newParser = new Parser()
  const oldParser = parse('data.*')

  const start = Date.now()
  // await testIt(['[{"id": "\\\\"}]'], 1)
  fs.createReadStream('MAHR.json')
    .pipe(newParser)
    // .pipe(oldParser)
    .on('data', (product) => {
      i++
    })
    .on('end', () => {
      const seconds = ((Date.now() - start) / 1000)
      clearInterval(interval)
      log()
      console.log(`Processed [${i}] items after [${Math.round(seconds)}] with average speed [${Math.round(i / seconds)}] items per second.`)
    })
}

f().catch(console.error)

