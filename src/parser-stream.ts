import { Duplex, PassThrough } from 'stream'
import Parser from './parser'
import { Worker } from 'worker_threads'
import path from 'path'

export default class ParserStream extends Duplex {
  private readonly pt: Duplex
  private readonly parser: Parser

  constructor () {
    super({
      writableObjectMode: false,
      readableObjectMode: true,
      write: (chunk: any, encoding: string, callback: (error?: (Error | null)) => void): void => {
        this.pt.write(chunk, encoding, callback)
      },
      read (size: number): void {
        let i = 5
      },
      destroy (error: Error | null, callback: (error: (Error | null)) => void): void {
        callback(error)
      },
      final (callback: (error?: (Error | null)) => void): void {
        callback()
      },
    })
    this.pt = new PassThrough()
    this.on('end', () => {
      this.pt.end()
    })
    this.parser = new Parser(this.pt[Symbol.asyncIterator](), 1024 * 1024 * 50, 1)
    this.loop().catch(reason => {
      this.destroy(reason)
    })
  }

  private async loop () {
    let nextObject: Buffer | null
    const workerCount = 1
    let currentWorker = 0
    let endWatchDog = 0
    let counter = 0
    const workers: Worker[] = []
    // Init workers
    for (let i = 0; i < workerCount; i++) {
      const worker = new Worker(path.join(__dirname, 'workerwrapper.js'))
      worker.on('message', (msg) => {
        counter--
        this.push(msg)
      })
      workers.push(worker)
    }
    function getWorker (): number {
      return currentWorker = ++currentWorker % workers.length
    }
    // Finalizer
    const finish = async () => {
      if (endWatchDog > 100) {
        throw new Error(`Something went wrong. Did not receive expected amount of objects and [${counter}] left.`)
      }
      if (counter === 0) {
        this.push(null)
        for (let worker of workers) {
          await worker.terminate()
        }
      } else {
        endWatchDog++
        setTimeout(finish, 100)
      }
    }
    // Main loop
    while (nextObject = await this.parser.nextObject()) {
      counter++
      if (workerCount) {
        workers[getWorker()].postMessage(nextObject)
      } else {
        counter--
        this.push(ParserStream.parse(ParserStream.stringify(nextObject)))
      }
    }
    // Finish
    await finish()
  }

  private static stringify (buf: Buffer): string {
    return buf.toString()
  }

  private static parse (str: string): Object {
    return JSON.parse(str)
  }

}
